﻿#Данный скрипт запускается удаленно и выводит информацию о загрузке ПК в реальном времени построчно, далее можно отформатировать лог файл в excel и составить график из полученных значений.
#Естественно у вас должен быть доступ на указанный ПК. К примеру, что бы получить доступ до ПК оператора, я запускал ISE под чужой УЗ.
#----------
$hostname = "Имя ПК" #Тут в кавычках вводим Имя ПК, пример "PC-A00502313"
Start-Transcript -Append "C:\Путь_до_места_логирования\Имя_ПК.log" #запуск логирования, путь указать свой
#Запускаем скрипт на указанном ПК (удаленно)
Invoke-Command -ComputerName $hostname -ScriptBlock {
#создаем переменную отсчета для цикла
$cycle = 0
#Для информации выводим имя ПК на котором запущен монитор
HOSTNAME.EXE
    # запускаем цикл, указано 1800 циклов - это примерно 2 часа
    WHILE ($cycle -lt 1800){
        #По очереди запускаем показания: Время; CPU; Mem; HDD
        Get-Counter '\процессор(_total)\% загруженности процессора' | select -expand Timestamp | ForEach{Write-Host $_} #time
        Get-Counter '\процессор(_total)\% загруженности процессора' | select -expand CounterSamples | ForEach{Write-Host $_.CookedValue -fore Red} #CPU
        Get-Counter '\память\% использования выделенной памяти' | select -Expand CounterSamples | ForEach{Write-Host $_.CookedValue -fore Yellow} #Mem
        Get-Counter '\физический диск(_total)\% активности диска' | select -Expand CounterSamples | ForEach{Write-Host $_.CookedValue -fore  Cyan} #HDD
        Write-Host "***********" #разделитель
        $cycle = $cycle + 1 #Прибавляем к циклу + 1
        }
}
Stop-Transcript
#----------