﻿$cycle = 0
HOSTNAME.EXE
WHILE ($cycle -lt 1800){
    $Timestamp = Get-Counter '\процессор(_total)\% загруженности процессора' | select -expand Timestamp | ForEach{Write-Host $_}
    $LoadCPU = Get-Counter '\процессор(_total)\% загруженности процессора' | select -expand CounterSamples | ForEach{Write-Host $_.CookedValue -fore Red}
    $Mem = Get-Counter '\память\% использования выделенной памяти' | select -Expand CounterSamples | ForEach{Write-Host $_.CookedValue -fore Yellow}
    $HDD = Get-Counter '\физический диск(_total)\% активности диска' | select -Expand CounterSamples | ForEach{Write-Host $_.CookedValue -fore  Cyan}
    Write-Host `t
    $cycle = $cycle + 1
    }